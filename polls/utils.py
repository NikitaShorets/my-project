from django.core.checks import register


@register.filter(is_safe=True, needs_autoescape=True)
def rng(value: int, autoescape=True):
    """
    Convert all newlines in a piece of plain text to HTML line breaks
    (``<br>``).
    """
    return range(int(value))
