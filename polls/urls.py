from django.urls import path

from polls.views import PollsView, ChoiceView, LoginViews

# 127.0.0.1:8000/polls/1/choice/1

urlpatterns = [
    path("<int:poll_id>/choice/<int:choice_id>", ChoiceView.as_view()),
    path('login', LoginViews.as_view()),
    path("", PollsView.as_view()),
]