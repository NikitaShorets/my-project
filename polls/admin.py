from django.contrib import admin
from polls.models import Poll, Choice


class ChoiceInline(admin.StackedInline):
    model = Choice
    extra = 1


@admin.register(Poll)
class PollAdmin(admin.ModelAdmin):
    inlines = [ChoiceInline]
